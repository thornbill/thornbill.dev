---
title: Work In Progress — October 2021
tags:
  - jellyfin
  - mobx
  - new project
  - wip
date: 2021-11-16
---

Here are some of the open-source projects I have been working on in the month of October.

Eventually this has to stop, but I did set another personal record for the number of pull requests I reviewed in a month.
I reviewed a total of **122 pull requests** across 8 repositories! 🚀

## mobx-sync-lite

I started a new project this month called `mobx-sync-lite`!

[MobX](https://mobx.js.org) is a state management library for JavaScript/TypeScript projects.
`mobx-sync-lite` is a hard-fork of the `mobx-sync` library that is used to sync MobX state to a storage backend.
I started using `mobx-sync` in `jellyfin-expo` and I really like the simple API and feature set that it provides.
Unfortunately, the original author had [no plans to continue support](https://github.com/acrazing/mobx-sync/issues/43) due to some significant changes made in the latest version of MobX.
This fork aims to continue development and add support for new versions of MobX (eventually).

* [mobx-sync-lite#1](https://github.com/thornbill/mobx-sync-lite/pull/1): Fix the example project
* [mobx-sync-lite#3](https://github.com/thornbill/mobx-sync-lite/pull/3): Add dependabot and github actions
* [mobx-sync-lite#15](https://github.com/thornbill/mobx-sync-lite/pull/15): Ignore major version updates for mobx
* [mobx-sync-lite#16](https://github.com/thornbill/mobx-sync-lite/pull/16): Add rollup build check
* [mobx-sync-lite#17](https://github.com/thornbill/mobx-sync-lite/pull/17): Add badges and compatibility table to README

## Jellyfin

The first alpha release of Jellyfin 10.8 [has been released](https://old.reddit.com/r/jellyfin/comments/qk2ekf/the_first_alpha_for_1080_is_up_please_read_and/)!

Most of my Jellyfin time has been working on updating dependencies and making some quality of life changes for the Jellyfin iOS app (jellyfin-expo).
I also fixed a few small bugs in jellyfin-web for 10.8.

### jellyfin-expo

* [jellyfin-expo#297](https://github.com/jellyfin/jellyfin-expo/pull/297): Add lint rules
* [jellyfin-expo#298](https://github.com/jellyfin/jellyfin-expo/pull/298): Fix import order warnings
* [jellyfin-expo#299](https://github.com/jellyfin/jellyfin-expo/pull/299): Fix eslint issues on save in vscode
* [jellyfin-expo#300](https://github.com/jellyfin/jellyfin-expo/pull/300): Update codecov config to be less strict on coverage changes
* [jellyfin-expo#301](https://github.com/jellyfin/jellyfin-expo/pull/301): Update to Expo 42
* [jellyfin-expo#303](https://github.com/jellyfin/jellyfin-expo/pull/303): Remove feature request link
* [jellyfin-expo#304](https://github.com/jellyfin/jellyfin-expo/pull/304): Update icons
* [jellyfin-expo#305](https://github.com/jellyfin/jellyfin-expo/pull/305): Update dependencies
* [jellyfin-expo#306](https://github.com/jellyfin/jellyfin-expo/pull/306): Update react-navigation
* [jellyfin-expo#307](https://github.com/jellyfin/jellyfin-expo/pull/307): Add beta badge
* [jellyfin-expo#308](https://github.com/jellyfin/jellyfin-expo/pull/308): No scroll indicators
* [jellyfin-expo#311](https://github.com/jellyfin/jellyfin-expo/pull/311): Update AsyncStorage
* [jellyfin-expo#312](https://github.com/jellyfin/jellyfin-expo/pull/312): Remove deprecated device id
* [jellyfin-expo#315](https://github.com/jellyfin/jellyfin-expo/pull/315): Remove duplicate eslint override
* [jellyfin-expo#316](https://github.com/jellyfin/jellyfin-expo/pull/316): Use mobx-react-lite
* [jellyfin-expo#317](https://github.com/jellyfin/jellyfin-expo/pull/317): Use mobx-sync-lite

### jellyfin-web

* (In Review) [jellyfin-web#3017](https://github.com/jellyfin/jellyfin-web/pull/3017): Fix header centering on large mobile devices
* (In Review) [jellyfin-web#3018](https://github.com/jellyfin/jellyfin-web/pull/3018): Fix audio player overlapping on small screens
* (In Review) [jellyfin-web#3019](https://github.com/jellyfin/jellyfin-web/pull/3019): Enable multiserver in development environments

## jellyfin-sdk-typescript

As promised last month, I did write a [dedicated post](/posts/introducing-jellyfin-sdk-ts/) about the reasoning behind and current state of the TypeScript SDK for Jellyfin.

Some new functionality for finding the best potential server address and getting image URLs was added to the SDK.
There were also some changes made to how URLs were being parsed internally.
The SDK should be in good state for anyone to start using it and begin reporting any issues or missing features!

* [jellyfin-sdk-typescript#26](https://github.com/thornbill/jellyfin-sdk-typescript/pull/26): Add server recommendations
* [jellyfin-sdk-typescript#42](https://github.com/thornbill/jellyfin-sdk-typescript/pull/42): Remove usage of parse-url
* [jellyfin-sdk-typescript#43](https://github.com/thornbill/jellyfin-sdk-typescript/pull/43): Add method for getting an item image url

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
