---
title: Work In Progress — June 2021
tags:
  - interdoc
  - jellyfin
  - ossgit
  - wip
date: 2021-07-02
---

Happy Independence Day 🇺🇸 to those in the US and a happy (belated) Canada Day 🇨🇦 to our friends to the north!

Here are some of the open-source projects I have been working on in the month of June.

## Interdoc &mdash; A &ldquo;new&rdquo; open-source project

Interdoc is a project that I wrote several years ago and I finally decided to release it under an open-source license.
Interdoc is a RESTful API and minimal web interface for converting HTML and/or Markdown to a multitude of different formats including DOCX, EPUB, LaTeX, ODT, PDF, various wiki formats, and more!
The project relies on the powerful [Pandoc](https://pandoc.org/) command line utility on the backend.
I primarily used this to convert from Markdown to MediaWiki Markup because I could never get the syntax right. 😂

Considering the source had been sitting untouched for **three years**, getting everything up to date on a modern version of Node and updated dependencies was fairly painless.
A demo is currently hosted at [interdoc.thornbill.dev](https://interdoc.thornbill.dev/) and the source can be found on [my GitLab profile](https://gitlab.com/thornbill/interdoc).
Hopefully someone will find it useful!

## Jellyfin

This month I made it a priority to review pull requests for the projects I work on regularly for Jellyfin.
In particular jellyfin-web has fallen a bit behind with reviews and dependabot can be a bit ruthless.
In total it looks like I reviewed **95** pull requests this month.
Unsurprisingly most of those were in jellyfin-androidtv (41) and jellyfin-web (41). 🚀

### jellyfin-androidtv

We are still working towards that fabled 0.12 release for Android TV.
To that end, a lot of the ongoing work on the app has been focused on polish and stability.

This month I have worked on updating the appearance of the user and clock view shown in the upper right corner of the screen so that it matches the new toolbar used on the home screen.
Also the player overlay received some minor layout tweaks for the text and logo as well as a fix for the positioning of the audio delay popup.

* [jellyfin-androidtv#920](https://github.com/jellyfin/jellyfin-androidtv/pull/920): Refactor clock view
* [jellyfin-androidtv#921](https://github.com/jellyfin/jellyfin-androidtv/pull/921): Fix nullability issues
* [jellyfin-androidtv#943](https://github.com/jellyfin/jellyfin-androidtv/pull/943): Improve null handling in PlaybackHelper.getItemsToPlay
* [jellyfin-androidtv#947](https://github.com/jellyfin/jellyfin-androidtv/pull/947): Fix broken layout on grid pages
* [jellyfin-androidtv#966](https://github.com/jellyfin/jellyfin-androidtv/pull/966): Update clock bug to match toolbar
* [jellyfin-androidtv#973](https://github.com/jellyfin/jellyfin-androidtv/pull/973): Update playback overlay layout
* (In Review) [jellyfin-androidtv#977](https://github.com/jellyfin/jellyfin-androidtv/pull/977): Fix user information not reflecting updates
* [jellyfin-androidtv#978](https://github.com/jellyfin/jellyfin-androidtv/pull/978): Fix crash when watching and recording live tv
* [jellyfin-androidtv#979](https://github.com/jellyfin/jellyfin-androidtv/pull/979): Fix crash when previous channel id is empty
* [jellyfin-androidtv#980](https://github.com/jellyfin/jellyfin-androidtv/pull/980): Add glide app module for configuration
* [jellyfin-androidtv#981](https://github.com/jellyfin/jellyfin-androidtv/pull/981): Fix crash due to missing context on grid
* [jellyfin-androidtv#983](https://github.com/jellyfin/jellyfin-androidtv/pull/983): Update background filter transparency
* [jellyfin-androidtv#993](https://github.com/jellyfin/jellyfin-androidtv/pull/993): Fix audio delay popup positioning
* [jellyfin-androidtv#994](https://github.com/jellyfin/jellyfin-androidtv/pull/994): Change default step value in NumberSpinner
* [jellyfin-androidtv#995](https://github.com/jellyfin/jellyfin-androidtv/pull/995): Remove unnecessary createPopupMenu util

### jellyfin-web

React support is in! 🎉
If you run unstable or the master branch of web, the search page is now a React component... and it looks exactly like it did previously.
It is another very exciting step forward to me though as it allows us to move past the limitations and issues inherit with building out all pages in the app using DOM manipulation and HTML string injection.

Another feature I worked on this month was adding the ability to define custom links to show up in the menu in Jellyfin.
It was apparent after our move to the current webpack build system that a lot of people were manipulating the source files of their Jellyfin install to achieve this.
With my change, links can now be defined in the web config.json file.
Additional details can be found in the [documentation](https://jellyfin.org/docs/general/clients/web-config.html#custom-menu-links).

* [jellyfin-web#2683](https://github.com/jellyfin/jellyfin-web/pull/2683): Add React support
* [jellyfin-web#2698](https://github.com/jellyfin/jellyfin-web/pull/2698): Fix live reload
* [jellyfin-web#2703](https://github.com/jellyfin/jellyfin-web/pull/2703): Add error logging for missing translation keys
* [jellyfin-web#2704](https://github.com/jellyfin/jellyfin-web/pull/2704): Fix continue reading card shape
* [jellyfin-web#2705](https://github.com/jellyfin/jellyfin-web/pull/2705): Add support for custom menu links in config.json
* [jellyfin-web#2706](https://github.com/jellyfin/jellyfin-web/pull/2706): Update icon used for syncplay
* [jellyfin-web#2736](https://github.com/jellyfin/jellyfin-web/pull/2736): Fix invalid import path
* [jellyfin-web#2742](https://github.com/jellyfin/jellyfin-web/pull/2742): Update dependencies
* [jellyfin-web#2743](https://github.com/jellyfin/jellyfin-web/pull/2743): Remove html from syncplay toast messages
* (In Review) [jellyfin-web#2758](https://github.com/jellyfin/jellyfin-web/pull/2758): Use album artist for listing artist albums
* (Awaiting Backport) [jellyfin-web#2759](https://github.com/jellyfin/jellyfin-web/pull/2759): Fix serviceworker paths

## thornbill.dev

You may notice I have made some updates to this website this month also!

* There is now an [RSS feed](/feed.xml) available so you can follow my posts.
* Post pages have been redesigned slightly and now sport a navigation header.
* Tag support has been added including a basic page listing [all tags](/tags/) for browsing.
* Licenses have been updated. All code for this site is available under the [BSD-2-Clause License](https://gitlab.com/thornbill/thornbill.dev/-/blob/master/LICENSE) and all other content is provided under [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) unless otherwise specified.

The source for this site is also available on [my GitLab profile](https://gitlab.com/thornbill/thornbill.dev).

## ossgit

Things have been a little quiet for [ossgit.org](https://ossgit.org) this month.
There are a few new repositories being mirrored there including the newest project to join the Jellyfin organization: [Swiftfin](https://github.com/jellyfin/swiftfin).
Otherwise there is not much to report on this front. 🦗

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
An option for custom amounts and one-time contributions is now available.
