---
title: Favorite Things of 2023
tags: favorite things
date: 2024-01-16
---

I started making lists of my favorite (new to me) things for the year 2020, inspired by the series from [Tested](https://www.tested.com/).
I have always had the intention of publishing these lists to this blog, but I was uncertain if anyone would find them interesting.
If nothing else, publishing these here will be easier for me to reference than being locked away inside various note-taking apps.

## Hardware

1. [Vertical mouse](https://www.anker.com/products/a7852) &mdash; I have dealt with some wrist pain for some time that tends to flare up after long sessions of computer usage or driving. This strange-looking mouse seems to have alleviated a lot of that discomfort for me.

## Development

1. [TanStack (React) Query](https://tanstack.com/query/latest) &mdash; This library for server state management makes a lot of complex problems (caching, refreshing, and de-duping API requests) really simple.
2. [Zustand](https://docs.pmnd.rs/zustand) &mdash; This library is a refreshing take on client-side state management. It is really everything that I wanted from mobx, with official support for features like state persistence.

## Technology

1. [Mastodon](https://joinmastodon.org/) | [IceCubesApp](https://github.com/Dimillian/IceCubesApp) (app) &mdash; I have never been a very active user of Twitter, but the anti-features that have been rolled out in the past 1-2 years have led me to completely stop using the service this year. Mastodon was not entirely new to me. (I had an account several years ago on a now-defunct server.) However, this year I setup a new account (thanks to the folks at [Fosstodon](https://fosstodon.org) for hosting their public instance!) and started using the excellent Ice Cubes app as a client on iOS. There may not be as much content available as there was on Twitter, but I really don't care as I find the quality of content much higher (at least for my general interests in open-source software and self-hosting).
2. RSS | [FreshRSS](https://www.freshrss.org/) (server) | [NetNewsWire](https://netnewswire.com/) (app) &mdash; Another service that effectively died for me this year was Reddit. Their open hostility toward users, moderators, and third-party developers prompted me to stop using the service. This left me looking for a replacement for following current events, and I found myself returning to using RSS for the first time in several years. I set up FreshRSS to use as an RSS aggregator and web reader. The experience is much nicer than the aggregator I used years ago. Coupled with NetNewsWire on iOS, it has _mostly_ replaced the use case I had for Reddit, but entirely using open-source software and open web standards!
3. [Seek](https://www.inaturalist.org/pages/seek_app) (app) &mdash; This is probably a bit of an outlier compared to the other apps on this list. Seek is an augmented reality-type app that attempts to identify animal and plant life. I have found it very useful while on hikes and outings with Scouts or just while in my yard.

## Entertainment

1. [Jury Duty](https://www.themoviedb.org/tv/222023-jury-duty) &mdash; This is just a fun situational comedy show with a different take on a "reality" style show.
2. [The Witcher](https://www.themoviedb.org/tv/71912-the-witcher) &mdash; I'm a bit late to this one and entirely unfamiliar with the related books and games, but I still really enjoyed this sci-fi/fantasy series. Although I am somewhat concerned about the next season's transition to a new actor playing the main role.
3. Ready Player One (book) &mdash; Definitely not a new book, but it was a really fun read. I had previously watched the movie, and while they are substantially different, I think the movie did a decent job of capturing the spirit of the book.
4. [Wargroove](https://wargroove.com/) &mdash; My son randomly picked up this game on the Xbox, and it immediately became one of my favorites. The game play reminds me a lot of the Advance Wars games for the Game Boy Advance, and the developer even lists this as a source of inspiration for them. They released a sequel (Wargroove 2) this year also, but I have not picked it up yet.

## Miscellaneous

1. Record player &mdash; I think that owning physical media is really important in the age of everything being digital and [ownership rights generally disappearing](https://pluralistic.net/2023/12/08/playstationed/). I was given a vintage record player in 2022, but I didn't really have speakers for it until the end of the year. Now I have it all setup in my office and have a few records from my favorite artists to switch out during my work day.
2. Hot Wheels Batmobiles &mdash; These are just desk toys for me to fidget with during meetings, because who doesn't like the Batmobile??? (My personal favorite is the Batmobile from Batman the Animated Series.)
3. [Funko Pop](https://funko.com/) &mdash; Yes, they can be rather silly-looking, but they are affordable and cover a huge range of different fandoms. I think they are fun.
4. [Bro Thor's New Asgard](https://brickset.com/sets/76200-1/Bro-Thor-s-New-Asgard) (LEGO set) &mdash; This is a small set that packs in a **ton** of detail, and "Bro Thor" is one of the funniest characters in the [Marvel Cinematic Universe](https://en.wikipedia.org/wiki/Marvel_Cinematic_Universe).
