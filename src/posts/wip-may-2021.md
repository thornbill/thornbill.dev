---
title: Work In Progress — May 2021
tags:
  - jellyfin
  - ossgit
  - wip
date: 2021-06-07
---

Here are some of the things I have been working on in the month of May.

## jellyfin-androidtv

Most of my time this month has been spent working on the Jellyfin Android TV app in hopes that the 0.12 update will release at some point in 2021… 🤞

I have mainly been focusing on UX issues and regressions in the app. This has included some improvements for subtitles, focus issues on Fire OS, and improving performance of rendering blur hash placeholders.

I also worked on refactoring the player profiles. This should make it easier for people to contribute fixes for issues when the profile is inaccurate.

* [jellyfin-androidtv#861](https://github.com/jellyfin/jellyfin-androidtv/pull/861): Improve subtitle rendering
* [jellyfin-androidtv#862](https://github.com/jellyfin/jellyfin-androidtv/pull/862): Add workaround for koin logging issue
* [jellyfin-androidtv#870](https://github.com/jellyfin/jellyfin-androidtv/pull/870): Fix null media url crash
* [jellyfin-androidtv#873](https://github.com/jellyfin/jellyfin-androidtv/pull/873): Add horizontal padding to subtitles
* [jellyfin-androidtv#874](https://github.com/jellyfin/jellyfin-androidtv/pull/874): Fix Fire OS focus issues on login
* [jellyfin-androidtv#875](https://github.com/jellyfin/jellyfin-androidtv/pull/875): Fix default value for DefaultView preference
* [jellyfin-androidtv#876](https://github.com/jellyfin/jellyfin-androidtv/pull/876): Change subtitle delivery method for dvdsubs in exoplayer
* [jellyfin-androidtv#877](https://github.com/jellyfin/jellyfin-androidtv/pull/877): Remove text dimming on inactive cards
* [jellyfin-androidtv#888](https://github.com/jellyfin/jellyfin-androidtv/pull/888): Refactor alpha picker in Kotlin and fix Fire OS compatability
* [jellyfin-androidtv#895](https://github.com/jellyfin/jellyfin-androidtv/pull/895): Fix subtitles out of bounds crash
* [jellyfin-androidtv#898](https://github.com/jellyfin/jellyfin-androidtv/pull/898): Remove transparency from card badges
* [jellyfin-androidtv#899](https://github.com/jellyfin/jellyfin-androidtv/pull/899): Refactor player profiles
* [jellyfin-androidtv#903](https://github.com/jellyfin/jellyfin-androidtv/pull/903): Grid view improvements
* [jellyfin-androidtv#905](https://github.com/jellyfin/jellyfin-androidtv/pull/905): Decode blur hashes in background thread
* [jellyfin-androidtv#907](https://github.com/jellyfin/jellyfin-androidtv/pull/907): Disable AC3 for Fire Stick Gen 1 devices
* [jellyfin-androidtv#911](https://github.com/jellyfin/jellyfin-androidtv/pull/911): Add flac support to exoplayer profile
* [jellyfin-androidtv#917](https://github.com/jellyfin/jellyfin-androidtv/pull/917): Allow wildcard imports in detekt
* [jellyfin-androidtv#919](https://github.com/jellyfin/jellyfin-androidtv/pull/919): Hide live TV section when unavailable
* (In Review) [jellyfin-androidtv#920](https://github.com/jellyfin/jellyfin-androidtv/pull/920): Refactor clock view
* (In Progress) [jellyfin-androidtv#921](https://github.com/jellyfin/jellyfin-androidtv/pull/921): WIP: Crash fixes

## jellyfin-web

Some very exciting changes are in the works on the Jellyfin Web side of things also.

For developers, I recently found a fix for live reload when running the site with the webpack dev server. I also have an open PR to add support for rendering pages with [React](https://reactjs.org/). Hopefully this will improve security and make it easier to develop new pages!

Speaking of security, a fix was made for an XSS vulnerability that was disclosed. This also prompted me to cleanup some of the code for toast notifications.

* [jellyfin-web#2647](https://github.com/jellyfin/jellyfin-web/pull/2647): Fix mov support in Safari
* [jellyfin-web#2648](https://github.com/jellyfin/jellyfin-web/pull/2648): Fix alpine python package
* [jellyfin-web#2657](https://github.com/jellyfin/jellyfin-web/pull/2657): Bump jellyfin-apiclient to 1.8.0
* [jellyfin-web#2675](https://github.com/jellyfin/jellyfin-web/pull/2675): Fix sharing url
* [jellyfin-web#2676](https://github.com/jellyfin/jellyfin-web/pull/2676): Fix xss via displaymessage
* [jellyfin-web#2681](https://github.com/jellyfin/jellyfin-web/pull/2681): Cleanup toast messages
* (In Review) [jellyfin-web#2683](https://github.com/jellyfin/jellyfin-web/pull/2683): Add React support
* [jellyfin-web#2698](https://github.com/jellyfin/jellyfin-web/pull/2698): Fix live reload

## jellyfin-apiclient-javascript

For the JavaScript apiclient, changes were made to make our npm publishes automated. This should prevent some of the issues we have had around releases of jellyfin-web in the past.

* [jellyfin-apiclient-javascript#208](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/208): Add GH Action for npm publishing
* [jellyfin-apiclient-javascript#209](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/209): Bump version to 1.8.0
* [jellyfin-apiclient-javascript#210](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/210): Remove unused typescript configuration
* [jellyfin-apiclient-javascript#211](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/211): Add dependabot config for GH Actions

## Other Jellyfin contributions

A few other minor changes were made to [jellyfin-expo](https://github.com/jellyfin/jellyfin-expo), [jellyfin/.github](https://github.com/jellyfin/.github), and [jellyfin](https://github.com/jellyfin/jellyfin) although nothing of note in my opinion. 😉

## ossgit

Just a couple updates on [ossgit](https://ossgit.org/). If you noticed any instability or long load times, there was a server misconfiguration causing some issues that has finally been resolved. ossgit has also been updated to use the latest release of Gitea.

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
