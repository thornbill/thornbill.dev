---
title: Jellyfin on iOS Hack Week Retrospective
tags: jellyfin
date: 2020-09-09
---

I decided to take the last week (30 August 2020) to hack on some of the issues with the iOS experience on recent versions of Jellyfin. Below is a summary of the progress made.

## Quick Wins in Jellyfin-Web

Support for Picture-in-Picture (PiP) will be properly detected in future Jellyfin versions! This will prevent the PiP control from showing in the video playback controls on devices when it is not actually supported by the device (iPhones and older iPads). See the [pull request](https://github.com/jellyfin/jellyfin-web/pull/1873) for additional details.

iPad detection will now be more robust in future Jellyfin versions! Recent iOS versions made it more difficult to detect if a website was opened on an iPad. It is important for Jellyfin to be able to accurately determine what device it is running on in order to properly report what media the device can support. This [pull request](https://github.com/jellyfin/jellyfin-web/pull/1880) added some logic to improve our ability to detect iPads in these cases.

## Playback Issues

If you have tried Jellyfin on an older iOS device and found that nothing would play, you will be glad to hear that a fix was found! There was an assumption in jellyfin-web that all iOS devices would support the AC-3 and E-AC-3 audio codecs. Based on this [support matrix](https://developer.dolby.com/platforms/apple/ios/device-support/), older iPhones and iPads lack support for these codecs though. A change was added in jellyfin-web to properly report those codecs as unsupported for these devices. More details can be found in the [pull request](https://github.com/jellyfin/jellyfin-web/pull/1893).

Another issue was discovered that caused progressive playback of audio transcoded to the AAC codec to fail to play. This was resolved by this [pull request](https://github.com/jellyfin/jellyfin/pull/4053) in the server repository.

Additional transcoding issues are still being investigated. If you are encountering video playback failures on iOS when transcoding, please provide details in this [issue](https://github.com/jellyfin/jellyfin/issues/3815) including the media format information and any server and ffmpeg logs.

## iPad Rotation in the Jellyfin App

The issue that causes iPads to switch orientation when launching the Jellyfin app was fixed upstream by a contributor to the Expo project. I had hoped we could upgrade the library and release a fix for the Jellyfin app, but Expo [did not release](https://github.com/expo/expo/issues/10055) the update for the current Expo SDK version. As a result, this will be on-hold until the next release of the Expo SDK.
