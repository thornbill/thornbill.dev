---
title: Work In Progress — September 2021
tags:
  - jellyfin
  - wip
date: 2021-10-05
---

Here are some of the open-source projects I have been working on in the month of September.

## Jellyfin

The biggest update for Jellyfin is that the 0.12 release of the Android TV app is finally live!
This was a massive effort taking over a year since the 0.11 release.
More information can be found on the [Jellyfin blog](https://jellyfin.org/posts/android-tv-12/).

> A quick note to any Fire TV users, at the time of writing the app is still pending approval from Amazon,
> but your devices should automatically update once it is approved.

[Jellyfin 10.7.7](https://github.com/jellyfin/jellyfin/releases/tag/v10.7.7) was also released in September.
The release consisted of a couple minor bugfixes and a fix for an issue where user settings could get in an invalid state which could prevent clients from loading.

I once again set a new personal record for the number of pull requests I reviewed in a month.
I reviewed a total of **119 pull requests** across 11 repositories! 🚀

### jellyfin-web

Most of my development time was spent trying to get jellyfin-web ready for a beta release of Jellyfin 10.8.
The biggest change needed was to fix the direct play logic to match some server-side changes.
Most pull requests were for minor improvements to make the user experience more polished.
One nice feature for iOS 15 users is the addition of theme color support in Safari.

* [jellyfin-web#2912](https://github.com/jellyfin/jellyfin-web/pull/2912): Disable warnings in webpack dev server overlay
* [jellyfin-web#2913](https://github.com/jellyfin/jellyfin-web/pull/2913): Reorder item details sections
* [jellyfin-web#2939](https://github.com/jellyfin/jellyfin-web/pull/2939): Log warning instead of error when dictionary has not loaded
* [jellyfin-web#2940](https://github.com/jellyfin/jellyfin-web/pull/2940): Fix direct play logic when direct stream is disabled
* [jellyfin-web#2943](https://github.com/jellyfin/jellyfin-web/pull/2943): Fix play all and shuffle buttons
* [jellyfin-web#2945](https://github.com/jellyfin/jellyfin-web/pull/2945): Fix media indicator color
* [jellyfin-web#2946](https://github.com/jellyfin/jellyfin-web/pull/2946): Disable sync correction by default on mobile
* [jellyfin-web#2948](https://github.com/jellyfin/jellyfin-web/pull/2948): Restore missing placeholder error message
* [jellyfin-web#3001](https://github.com/jellyfin/jellyfin-web/pull/3001): Update apiclient
* [jellyfin-web#3002](https://github.com/jellyfin/jellyfin-web/pull/3002): Add theme color support
* [jellyfin-web#3016](https://github.com/jellyfin/jellyfin-web/pull/3016): Remove unused sass mixins
* (In Review) [jellyfin-web#3017](https://github.com/jellyfin/jellyfin-web/pull/3017): Fix header centering on large mobile devices
* (In Review) [jellyfin-web#3018](https://github.com/jellyfin/jellyfin-web/pull/3018): Fix audio player overlapping on small screens
* (In Review) [jellyfin-web#3019](https://github.com/jellyfin/jellyfin-web/pull/3019): Enable multiserver in development environments
* [jellyfin-web#3021](https://github.com/jellyfin/jellyfin-web/pull/3021): Move the selected player name next to cast icon

### jellyfin-apiclient-javascript

The JavaScript API client needed a new version published to include some required changes for Jellyfin 10.8.
The first version did not properly target ES5 environments, so a fix for that was published as a bugfix release.

* [jellyfin-apiclient-javascript#288](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/288): Bump version to 1.9.0
* [jellyfin-apiclient-javascript#304](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/304): Fix babel target
* [jellyfin-apiclient-javascript#306](https://github.com/jellyfin/jellyfin-apiclient-javascript/pull/306): Bump version to 1.9.1

### jellyfin-androidtv

I only had a couple pull requests for the Android TV app this month to fix some last minute issues in the 0.12 release.

* [jellyfin-androidtv#1125](https://github.com/jellyfin/jellyfin-androidtv/pull/1125): Fix now playing bug layout
* [jellyfin-androidtv#1138](https://github.com/jellyfin/jellyfin-androidtv/pull/1138): Remove unused tile colors and update default theme

## jellyfin-sdk-typescript

This month I published a new project that I have been working on, a [TypeScript SDK for Jellyfin](https://github.com/thornbill/jellyfin-sdk-typescript).
This project is **not** (currently) an official Jellyfin project.
This was my first time doing much in TypeScript, and I am loving it.
More information about this project will be published in a separate post soon!

## thornbill.dev

I have made a few updates to this website this month also.
Since I updated the layout of the blog post pages, I have wanted to apply a similar layout to the home page, and now it has been updated.
I also added a footer to every page of the website that specifies the license.

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
