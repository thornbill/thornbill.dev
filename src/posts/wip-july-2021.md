---
title: Work In Progress — July 2021
tags:
  - jellyfin
  - ossgit
  - wip
date: 2021-08-04
---

Another month has passed, so it is time for another report of the projects I have been working on. I spent some time visiting family this month, so I made noticeably fewer contributions in July.

## Jellyfin

The beta release of Android TV app version 0.12 is now live! A huge shoutout to [Niels](https://github.com/nielsvanvelzen) for all his work making this release possible! 🎉 This release includes over a year of work including a completely revamped login workflow, design improvements throughout the app, and a ton of bug fixes. Additional information and installation instructions can be found on the [Jellyfin blog](https://jellyfin.org/posts/android-betas/).

In addition to the changes below, I reviewed 48 pull requests in 7 different Jellyfin repositories this month.

### jellyfin-androidtv

The changes for Android TV that I made this month were mostly polishing the user experience for the pending 0.12 release. The most significant was probably some playback profile fixes for Fire TV devices. The profile was limiting codec support to what was supported by the first generation Fire Stick devices when newer devices support additional codecs. There was also an issue where the maximum supported resolution was not included in the device profiles.

* [jellyfin-androidtv#993](https://github.com/jellyfin/jellyfin-androidtv/pull/993): Fix audio delay popup positioning
* [jellyfin-androidtv#994](https://github.com/jellyfin/jellyfin-androidtv/pull/994): Change default step value in NumberSpinner
* [jellyfin-androidtv#995](https://github.com/jellyfin/jellyfin-androidtv/pull/995): Remove unnecessary createPopupMenu util
* [jellyfin-androidtv#1013](https://github.com/jellyfin/jellyfin-androidtv/pull/1013): Playback fixes for Fire TVs
* [jellyfin-androidtv#1019](https://github.com/jellyfin/jellyfin-androidtv/pull/1019): Change default screen

### jellyfin-web

You may have noticed that sometimes after updating Jellyfin the web interface will not load properly until you refresh a few times or clear the browser cache. This was due to some issues with how caching was setup in our webpack configuration. After 10.8.0 this should no longer be an issue as the main JavaScript file name will not change, but will instead use a URL parameter for cache-busting. Hopefully this will make your update process go more smoothly. 🙂

* [jellyfin-web#2784](https://github.com/jellyfin/jellyfin-web/pull/2784): Fix ATV 0.11 using wrong icon
* [jellyfin-web#2789](https://github.com/jellyfin/jellyfin-web/pull/2789): Update stalebot configuration
* [jellyfin-web#2790](https://github.com/jellyfin/jellyfin-web/pull/2790): Fix webpack config for bundle caching

### jellyfin

I found a server issue introduced in 10.7 that caused the server to not honor a device's maximum supported resolution for video playback while working on the Android TV app. Luckily this was a pretty easy bug caused by an API migration, so I was able to submit a fix.

* [jellyfin#6274](https://github.com/jellyfin/jellyfin/pull/6274): Restore max width and height params
* [jellyfin#6300](https://github.com/jellyfin/jellyfin/pull/6300): Update stalebot configuration

## ossgit

[Ossgit](https://ossgit.org) has been updated to the latest version of Gitea once again. There were a few minutes of downtime during the update due to some template changes that needed to be made for the new version.

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
