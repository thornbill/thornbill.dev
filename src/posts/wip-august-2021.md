---
title: Work In Progress — August 2021
tags:
  - jellyfin
  - wip
date: 2021-09-13
---

It may be a little later than usual, but here is a summary of the open-source contributions I made in August.

## Jellyfin

August was a (relatively) slow coding month for me. However I think I set a new personal record for the number of pull requests I reviewed. I reviewed a total of **107 pull requests** across 7 repositories in the Jellyfin organization! 🚀

### jellyfin-androidtv

The Jellyfin Android TV app is continuing to prepare for the final release of the 0.12 version of the app. The pull requests I worked on this month focused on improving codec support, fixing some bugs, and a little bit of code cleanup. We released three updates to the 0.12 beta in the month of August and are getting very close now to a final release.

* [jellyfin-androidtv#1076](https://github.com/jellyfin/jellyfin-androidtv/pull/1076): Allow user sessions to be set to null again
* [jellyfin-androidtv#1102](https://github.com/jellyfin/jellyfin-androidtv/pull/1102): Restore direct play support for avi with libvlc
* [jellyfin-androidtv#1103](https://github.com/jellyfin/jellyfin-androidtv/pull/1103): Enable multichannel audio support for Fire TVs
* [jellyfin-androidtv#1108](https://github.com/jellyfin/jellyfin-androidtv/pull/1108): Add guide card to live tv library views
* [jellyfin-androidtv#1110](https://github.com/jellyfin/jellyfin-androidtv/pull/1110): Downmix audio by default on Chromecast with Google TV
* [jellyfin-androidtv#1111](https://github.com/jellyfin/jellyfin-androidtv/pull/1111): Remove useless check method

### jellyfin-web

The changes I worked on in jellyfin-web this month focused mainly on developer experience. Several improvements were made to our CI workflow and reduce the amount of build time warnings that were produced. I also fixed a minor regression in unstable that caused the "continue watching" section of the home screen to show images in the wrong aspect ratio.

* [jellyfin-web#2821](https://github.com/jellyfin/jellyfin-web/pull/2821): Update merge conflict action trigger
* [jellyfin-web#2829](https://github.com/jellyfin/jellyfin-web/pull/2829): Fix continue watching card shapes
* [jellyfin-web#2848](https://github.com/jellyfin/jellyfin-web/pull/2848): Update dependabot frequency to run weekly
* [jellyfin-web#2853](https://github.com/jellyfin/jellyfin-web/pull/2853): Update workbox
* [jellyfin-web#2858](https://github.com/jellyfin/jellyfin-web/pull/2858): Fix build warnings

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
