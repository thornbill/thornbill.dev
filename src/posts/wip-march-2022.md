---
title: Work In Progress — March 2022
tags:
  - jellyfin
  - wip
date: 2022-04-12
---

It has been a little while since my last update&hellip; in addition to the usual holiday craziness,
I was dealing with some sickness at the end 2021 that seriously reduced my open-source activity.
Then at the end of January I started a position with a new employer.
It has taken me awhile to get back into the full swing of things,
but I believe I am getting there now.

Here are some of the open-source contributions I made in the month of March.

This month I have reviewed a total of 74 PRs in 10 repositories.

## Jellyfin

There have been some major new releases for Jellyfin projects in March!

Android TV v0.13 was released this month.
A huge shout out to [Niels](https://github.com/nielsvanvelzen), [Cameron](https://github.com/mueslimak3r), and all the other contributors for this release.
(I really only fixed one minor bug for this release.)
You can read the [release post](https://jellyfin.org/posts/android-tv-13/) for additional information about the new features and improvements.

The long awaited Beta 1 release for Jellyfin 10.8.0 is now available.
As usual, make sure you have proper backups available before trying any pre-release builds.
Details can be found in the [release announcement](https://old.reddit.com/r/jellyfin/comments/tpqrfm/the_first_beta_for_1080_is_up_were_getting_there/).

### jellyfin-web

The majority of my focus has been towards jellyfin-web recently.
A lot of this has been geared towards fixing minor issues in 10.8 in hopes of making it our most polished release to date.
A release branch for 10.8 was created as part of the release process for the first beta.
This means that work on 10.9 has already started on the master branch.
I have started working on some general code cleanup and minor refactoring as part of 10.9.
There is still a long way to go, but my hope is that cleaner code will foster new contributions in the future.

* [jellyfin-web#3538](https://github.com/jellyfin/jellyfin-web/pull/3538):
  Move screensavermanager to scripts
* [jellyfin-web#3537](https://github.com/jellyfin/jellyfin-web/pull/3537):
  Update libraries comments that reference Vue migration
* [jellyfin-web#3536](https://github.com/jellyfin/jellyfin-web/pull/3536):
  Remove setTitle function in app router
* [jellyfin-web#3534](https://github.com/jellyfin/jellyfin-web/pull/3534):
  Cleanup duplication in webpack dev config
* [jellyfin-web#3531](https://github.com/jellyfin/jellyfin-web/pull/3531):
  Remove broken method of registering routes for plugins
* [jellyfin-web#3527](https://github.com/jellyfin/jellyfin-web/pull/3527):
  Fix rewatching next up status
* [jellyfin-web#3498](https://github.com/jellyfin/jellyfin-web/pull/3498):
  Restore missing MessageUnauthorizedUser error message
* [jellyfin-web#3495](https://github.com/jellyfin/jellyfin-web/pull/3495):
  Fix any type warning
* [jellyfin-web#3493](https://github.com/jellyfin/jellyfin-web/pull/3493):
  Add default-case-last rule and fix issues
* [jellyfin-web#3489](https://github.com/jellyfin/jellyfin-web/pull/3489):
  Add headings to display settings screen
* [jellyfin-web#3480](https://github.com/jellyfin/jellyfin-web/pull/3480):
  Fix more accessibility issues
* [jellyfin-web#3478](https://github.com/jellyfin/jellyfin-web/pull/3478):
  Fix click on item details poster playing wrong item
* [jellyfin-web#3473](https://github.com/jellyfin/jellyfin-web/pull/3473):
  Move rewatching in next up to display option and remove home section

### jellyfin-expo

I made some updates to the jellyfin-expo project to support Jellyfin 10.8, fix some minor issues and warnings,
improve test coverage, and make some tooling updates.
A new app release will happen sometime before Jellyfin 10.8 is released to ensure compatibility.

* [jellyfin-expo#356](https://github.com/jellyfin/jellyfin-expo/pull/356):
  Add basic render tests for screen components
* [jellyfin-expo#355](https://github.com/jellyfin/jellyfin-expo/pull/355):
  Update npm cache in GH actions
* [jellyfin-expo#354](https://github.com/jellyfin/jellyfin-expo/pull/354):
  Add tooling support for typescript
* [jellyfin-expo#353](https://github.com/jellyfin/jellyfin-expo/pull/353):
  Refactor fetch with timeout to separate utility
* [jellyfin-expo#352](https://github.com/jellyfin/jellyfin-expo/pull/352):
  Remove support for server versions <10.3
* [jellyfin-expo#350](https://github.com/jellyfin/jellyfin-expo/pull/350):
  Use uuids for internal server ids
* [jellyfin-expo#349](https://github.com/jellyfin/jellyfin-expo/pull/349):
  Add eslint rules for sonarqube issues
* [jellyfin-expo#348](https://github.com/jellyfin/jellyfin-expo/pull/348):
  Fix react-native version
* [jellyfin-expo#347](https://github.com/jellyfin/jellyfin-expo/pull/347):
  Fix native-stack warning
* [jellyfin-expo#345](https://github.com/jellyfin/jellyfin-expo/pull/345):
  Remove exitmenu as a supported feature

### jellyfin-sdk-typescript

Work on the TypeScript SDK is still ongoing.
I thought I was more or less finalized with the API of the SDK,
but the approach I had taken did not allow the SDK to be tree-shaken properly.
This was a pretty big issue due to the size of the generated client.
The new API can be seen in [jellyfin-sdk-typescript#149](https://github.com/thornbill/jellyfin-sdk-typescript/pull/149)
which is still in progress since I need to add some documentation for upgrading.

My plan is to work towards moving the SDK to the Jellyfin organization after the next release,
since this will (hopefully) be the last major change needed for the API.

* (In Progress) [jellyfin-sdk-typescript#149](https://github.com/thornbill/jellyfin-sdk-typescript/pull/149):
  Fix tree-shaking of the SDK
* [jellyfin-sdk-typescript#148](https://github.com/thornbill/jellyfin-sdk-typescript/pull/148):
  Remove unnecessary exports
* [jellyfin-sdk-typescript#147](https://github.com/thornbill/jellyfin-sdk-typescript/pull/147):
  Move output directory to lib

### jellyfin

I did manage to contribute a few small updates to the Jellyfin server codebase this month despite my lack of experience with C#.
I made some adjustments to the behavior of the new "rewatching" feature in the next up home section
(although it is still buggy in some cases).
I also made some updates to how studio images are provided and where they are sourced from.

* [jellyfin#7423](https://github.com/jellyfin/jellyfin/pull/7423):
  Remove unused poster support from StudiosImageProvider
* [jellyfin#7396](https://github.com/jellyfin/jellyfin/pull/7396):
  Update artwork repository urls
* [jellyfin#7383](https://github.com/jellyfin/jellyfin/pull/7383):
  Include played and unplayed results in the same next up request

### jellyfin-androidtv

* [jellyfin-androidtv#1560](https://github.com/jellyfin/jellyfin-androidtv/pull/1560):
  Fix crash getting badge image with null base item

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
