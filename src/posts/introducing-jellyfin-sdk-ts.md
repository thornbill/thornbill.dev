---
title: A New TypeScript SDK for Jellyfin
tags:
  - jellyfin
  - new project
  - typescript
date: 2021-10-13
---

I recently published a new project that I have been working on, a [TypeScript SDK for Jellyfin](https://github.com/thornbill/jellyfin-sdk-typescript).
This project is **not** (currently) an official Jellyfin project.
I wanted the project to have a solid set of basic functionality before proposing it to be moved into the organization.
This allows me some freedom in making larger changes as I deem necessary while the project is still in early development.

The goal of the SDK is to provide a generated API client with some additional helpers and utility functions for **all** TypeScript and JavaScript environments (browsers, node.js, React Native, etc.).
It is **very** heavily inspired by the excellent work [@Niels](https://github.com/nielsvanvelzen) has done on the [Kotlin SDK](https://github.com/jellyfin/jellyfin-sdk-kotlin).

## Why another SDK / API client?

I was motivated to start working on a new TypeScript SDK, because in my opinion, the current API clients had some pretty fundamental flaws.

The [Jellyfin API Client for JavaScript](https://github.com/jellyfin/jellyfin-apiclient-javascript) began as a fork of an Emby project that is used throughout jellyfin-web.
The project relies heavily on browser APIs which makes it poorly suited for a modern JavaScript ecosystem where JavaScript is run in environments such as servers and mobile applications.
It also includes utilities that seem completely unrelated to interacting with a Jellyfin server such as an event bus implementation and a data store.
There is no way for changes to the Jellyfin API to be automatically added to this API client.
Any change in the API must be made manually which is tedious and error prone.
The project is also very poorly organized.
It consists of thousands of lines of JavaScript but is only divided into **six** files.
Finally, very little documentation is available to help other projects make use of this API client.

Next is the [Jellyfin API Client for Axios](https://github.com/jellyfin/jellyfin-client-axios).
This API client was just recently created by [@ferferga](https://github.com/ferferga) and is a massive improvement over the older JavaScript API client.
The project is automatically generated from the Jellyfin OpenAPI specification.
This means that any API changes will be included in the client without requiring any manual changes.
This is great, but... it _only_ provides the generated client.
That means that any application using the client needs to write a lot of boilerplate and utility functions for common functionality.

I thought the best option would be to use some of what we learned from the Jellyfin API Client for Axios and make it more like the Kotlin SDK.
The core could be generated from the OpenAPI specification but with all the boilerplate and some common utilities included.

## What is the current state of the project?

The new TypeScript SDK is at a point where it should be usable and fairly stable.
It can be installed from [npm](https://www.npmjs.com/package/@thornbill/jellyfin-sdk) today!
A basic demo of how to use the SDK can be found in this [CodePen](https://codepen.io/thornbill/full/mdwjVEo).
There is also [usage documentation](https://github.com/thornbill/jellyfin-sdk-typescript/blob/095b469cecf8592e9ae566ae62fd16ad8b5c51ad/README.md#usage) in the README and the [integration tests](https://github.com/thornbill/jellyfin-sdk-typescript/blob/15329b3eb97040dc60481cceb453462189865683/src/__tests__/integration.test.ts) can be used as a reference.
TypeDoc documentation is available in the [repository](https://github.com/thornbill/jellyfin-sdk-typescript/blob/386ca8ffbf452bb42134aedbacc8c8370365713f/docs/modules/index.md).

The SDK currently includes utilities for authentication and server discovery and validation.
Also included is initial support for device playback profile generation added by [@ThibaultNocchi](https://github.com/ThibaultNocchi).
Generating a complete profile is still a long ways off, but it will be great to handle device profiles in a common place as they are notoriously difficult to get right.
I am currently working on a utility to generate item image URLs, so that should be available in the next release.

If you are working on a TypeScript or JavaScript project that integrates with Jellyfin, I would encourage you to give my new SDK a try.
Of course if you think the SDK is missing some utility or find a bug, feel free to [open an issue](https://github.com/thornbill/jellyfin-sdk-typescript/issues).
Hopefully this will make it much easier for anyone who wants to make a project connect to Jellyfin!

---

If you are interested in supporting my work, you can [sponsor me on GitHub](https://github.com/sponsors/thornbill).
