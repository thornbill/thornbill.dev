const rssPlugin = require('@11ty/eleventy-plugin-rss');
const now = new Date();

module.exports = config => {
	config.addPlugin(rssPlugin);

	config.addFilter('dateYYYYMMDD', date => (
		date instanceof Date ?
			`${ date.getUTCFullYear() }-${ String(date.getUTCMonth() + 1).padStart(2, '0') }-${ String(date.getUTCDate()).padStart(2, '0') }` : ''
	));

	config.addFilter('first', (list, length = 1) => list.slice(0, length));

	config.addCollection('posts', collection =>
		collection
			.getFilteredByGlob('./src/posts/**/*.md')
			.filter(p => p.date <= now)
	);

	config.addFilter('filterTagList', tags => {
		// should match the list in tags.njk
		return (tags || []).filter(tag => ['all', 'nav', 'post', 'posts'].indexOf(tag) === -1);
	})

	// Create an array of all tags
	config.addCollection('tagList', collection => {
		let tagSet = new Set();
		collection.getAll().forEach(item => {
			(item.data.tags || []).forEach(tag => tagSet.add(tag));
		});

		return [...tagSet];
	});

	return {
		dir: {
			input: 'src'
		}
	};
};
