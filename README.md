# thornbill.dev

This is the source for my personal site and blog [thornbill.dev](https://thornbill.dev).

All code is licensed under the [BSD-2-Clause](./LICENSE) and all other site content
and assets are licensed under [CC-BY-NC-ND](./LICENSE.CONTENT) unless specified otherwise.
